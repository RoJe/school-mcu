#include <p16f1829.inc>
    ; set the correct configs with the right configuration.
    __CONFIG _CONFIG1, (_FOSC_INTOSC & _WDTE_OFF & _PWRTE_OFF & _MCLRE_OFF & _CP_OFF &  _CPD_OFF & _BOREN_ON & _CLKOUTEN_OFF & _IESO_OFF & _FCMEN_OFF);
    __CONFIG _CONFIG2, (_WRT_OFF & _PLLEN_OFF & _STVREN_OFF & _LVP_OFF);
   
    errorlevel -302 ;supress the not in bank0 warning as we're not starting at back 0
    cblock 0x70 ;0x70 is a shared databank so we're going to just spank the three file registers here.
	Delay1  ; The first delay variable
	Delay2  ; The second delay variable
	Delay3	; The third delay variabel
    endc
    
    ORG 0 ; Set the origin point here
    
    ; Initial configuration
    Start:
    
	;( bank select ) bank1
	banksel	OSCCON	
	
	; ( Move literal ( to ) Work register )
	;
	; Set the clock speed to 500kHz
	; Fosc	= 1 / Fosc = 1/500000 = 0.000002 = 2000ns
	; Tcy	= 4 * Tosc = 4*0.000002 = 0.000008 = 8000ns
	; Fcy	= 1 / Tcy  = 1/0.0000008 = 125000Hz = 125kHz
	; movlw = 1 * Tcy = 8000ns  
	movlw	b'00111000'
	
	; ( Move Work Register ( to ) File ) 
	; Move the Work Register to file OSCCON
	; movwf = 1 * Tcy = 8000ns  
	movwf	OSCCON
	
	; ( Bit Clear ( in ) file ) 
	; Make IO pin C0 an output from DS1
	; bcf = 1 * Tcy = 8000ns 
	bcf	TRISC, 0
	
	; ( Bit Clear ( in ) file )
	; Make IO pin C3 an output from DS3
	; bcf = 1 * Tcy = 8000ns 
	bcf	TRISC, 3
	
	; ( Bank select ) bank2
	banksel LATC	    
	
	; ( Clear file register ) 
	; Turn off all the LEDS
	; clrf = 1 * Tcy = 8000ns 
	clrf	LATC
	
	; ( Move literal ( to ) Work register )
	; Move the literual 81 to Working Register
	; movlw = 1 * Tcy = 8000ns 
	movlw	.81
	
	; ( Move Work Register ( to ) File )
	; Insert the moved literual to file Delay2
	; movwf = 1 * Tcy = 8000ns 
	movwf	Delay2
	
	; ( Move literal ( to ) Work register )
	; Move the literual 43 to Working Register
	; movlw = 1 * Tcy = 8000ns 
	movlw	.13
	
	; ( Move Work Register ( to ) File )
	; Insert the moved literual to file Delay3
	; movwf = 1 * Tcy = 8000ns 
	movwf	Delay3
	
	; ( Bit set ( in ) file )
	; Turn on LED DS1 as it's the first thing we should do.
	; bsf = 1 * Tcy = 8000ns
	bsf	LATC, 0	    
    DelayLoopOne:
	; ( Decrement file, skip if zero )
	; Decrement Delay1 by 1
	; decfsz = 1 * Tcy = 8000ns 
	decfsz	Delay1, f
	
	
	; ( bra is a small jump in this case to the location of DelayLoopOne )
	; decfsz = 2 * Tcy = 16000ns
	bra	DelayLoopOne
	
	
	; ( Decrement file, skip if zero )
	; Decrement Delay2 by 1
	; decfsz = 1 * Tcy = 8000ns
	decfsz	Delay2, f
	
	; ( bra is a small jump, in this case to the location of DelayLoopOne )
	; (768+3) * 81 = 62451 / 125K files per second APPROX 0.5 seconds ( 62451 / 81 = 0.499608 seconds )
	; decfsz = 2 * Tcy = 16000ns
	bra	DelayLoopOne 
	
	; Because 0.499608 != 0.5 we need to introduce an extra delay that does not get repeated within the loop multiple times. 
	; Delay3 =  .39, 0.499608 + 392 = 0.5, 392/8ns = 49.
	; Because we have the BRA's from above to take into acount and the loop from below we have to calculate  
	; becuase we have 6 instructions below and 4 BRA's above, that is 10 cycles. therefor that 49 turns into a 39
	;
	; Now, we know that BRA takes 2 cycles and decfsz takes 1 cycle. therefor we know that Delay3 is 13.
	    DelayLoopOneThree:
		; ( Decrement file, skip if zero )
		; Decrement Delay3 by 1
		; decfsz = 1 * Tcy = 8000ns
		decfsz	Delay3, f
		
		; ( bra is a small jump in this case to the location of DelayLoopOneThree )
		; decfsz = 2 * Tcy = 16000ns
		bra	DelayLoopOneThree
	
	; ( Bit Clear ( in ) file )
	; Turn off LED1
	; bcf = 1 * Tcy = 8000ns
	bcf	LATC, 0
	
	; ( Bit set ( in ) file )
	; Turn on LED 4
	; bsf = 1 * Tcy = 8000ns
	bsf	LATC, 3
	
	; ( Move literal ( to ) Work Register )
	; Move the literual 81 to Working Register
	; movlw = 1 * Tcy = 8000ns
	movlw	.81
	
	; ( Move Work Register ( to ) File )
	; Insert the moved literual to file Delay2
	; movwf = 1 * Tcy = 8000ns
	movwf	Delay2
	
	; ( Move literal ( to ) Work Register )
	; Move the literual .12 to Working Register for DelayLoopTwo
	; movlw = 1 * Tcy = 8000ns
	movlw	.12
	
	; ( Move Work Register ( to ) File )
	; Insert the moved literual to file Delay3
	; movwf = 1 * Tcy = 8000ns
	movwf	Delay3
    DelayLoopTwo:
    
	; ( Decrement file, skip if zero )
	; Decrement Delay1 by 1
	; decfsz = 1 * Tcy = 8000ns
	decfsz	Delay1, f
	
	
	; ( bra is a small jump in this case to the location of DelayLoopTwo )
	; decfsz = 2 * Tcy = 16000ns
	bra	DelayLoopTwo
	
	; ( Decrement file, skip if zero )
	; Decrement Delay1 by 1
	; decfsz = 1 * Tcy = 8000ns
	decfsz	Delay2, f
	
	; ( bra is a small jump in this case to the location of DelayLoopTwo )
	; decfsz = 2 * Tcy = 16000ns
	bra	DelayLoopTwo
	
	
	; Because 0.499608 != 0.5 we need to introduce an extra delay that does not get repeated within the loop multiple times. 
	; Delay3 =  .39, 0.499608 + 392 = 0.5, 392/8ns = 49.
	; Because we have the BRA's from above to take into acount and the loop from below we have to calculate  
	; becuase we have 6 instructions below and 4 BRA's above, one below, that is 12 cycles. therefor that 49 turns into a 37. 
	; as this is can't be devided by 3 we'll turn it into 36. this means we'll be 8000 miliseconds shy of the 0.5 seconds.
	;
	; Now, we know that BRA takes 2 cycles and decfsz takes 1 cycle. therefor we know that Delay3 is 12.
	    DelayLoopTwoThree:
		; ( Decrement file, skip if zero )
		; Decrement Delay3 by 1
		; decfsz = 1 * Tcy = 8000ns
		decfsz	Delay3, f
		
		; ( bra is a small jump in this case to the location of DelayLoopTwoThree )
		; decfsz = 2 * Tcy = 16000ns
		bra	DelayLoopTwoThree
	
	; ( Bit Clear ( in ) file )
	; Turn off LED4
	; bcf = 1 * Tcy = 8000ns
	bcf	LATC, 3
	
	; ( Bit set ( in ) file ) 
	; Turn on LED1
	; bsf = 1 * Tcy = 8000ns
	bsf	LATC, 0
	
	; ( Move literal ( to ) Work register )
	; Move the literual .81 to Working Register
	; movlw = 1 * Tcy = 8000ns
	movlw	.81
	
	; ( Move Work Register ( to ) File )
	; Insert the moved literual to file Delay2
	; movwf = 1 * Tcy = 8000ns
	movwf	Delay2
	
	; ( Move literal ( to ) Work register )
	; Move the literual .13 to Working Register for DelayLoopOne
	; movlw = 1 * Tcy = 8000ns
	movlw	.13
	
	; ( Move Work Register ( to ) File )
	; Insert the moved literual to file Delay3
	; movwf = 1 * Tcy = 8000ns
	movwf	Delay3
	
	; ( bra is a small jump, in this case to the location of DelayLoopOne )
	; decfsz = 2 * Tcy = 16000ns
	bra	DelayLoopOne
    end
    
